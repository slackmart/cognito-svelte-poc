provider "aws" {
  region = var.region
}

terraform {
  required_version = "=0.12.29"

  backend "s3" {
    bucket = "dou-cognito-poc"
    key    = "dou-cognito-poc-terraform/terraform.tfstate"
    region = "us-west-1"
  }
}
