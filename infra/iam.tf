resource aws_iam_policy "this" {
  name        = var.iam_policy_name
  description = "Gives read only access to s3 ${var.bucket_name} bucket"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetObject"
      ],
      "Resource": "arn:aws:s3:::${var.bucket_name}/public/*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource aws_iam_policy "thisprivate" {
  name        = "${var.iam_policy_name}-private"
  description = "Gives read only access to s3 ${var.bucket_name}/private"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
  {
    "Condition": {
       "StringLike": {
         "s3:prefix": [
           "private/",
           "private/*"
         ]
        }
      },
      "Action": "s3:ListBucket",
      "Resource": "arn:aws:s3:::${var.bucket_name}",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource aws_iam_role "auth_role" {
  name = var.auth_role_name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "cognito-identity.amazonaws.com"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "cognito-identity.amazonaws.com:aud": [
            "${aws_cognito_user_pool.this.id}",
            "${aws_cognito_identity_pool.this.id}"
          ]
        },
        "ForAnyValue:StringLike": {
          "cognito-identity.amazonaws.com:amr": "authenticated"
        }
      }
    }
  ]
}
EOF
}

resource aws_iam_role "unauth_role" {
  name = var.unauth_role_name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "cognito-identity.amazonaws.com"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "cognito-identity.amazonaws.com:aud": [
            "${aws_cognito_user_pool.this.id}",
            "${aws_cognito_identity_pool.this.id}"
          ]
        },
        "ForAnyValue:StringLike": {
          "cognito-identity.amazonaws.com:amr": "unauthenticated"
        }
      }
    }
  ]
}
EOF
}

resource aws_iam_role_policy_attachment "auth_s3_policy_attach_read_private" {
  policy_arn = aws_iam_policy.thisprivate.arn
  role       = aws_iam_role.auth_role.id
}

resource aws_iam_role_policy_attachment "unauth_s3_policy_attach_read" {
  policy_arn = aws_iam_policy.this.arn
  role       = aws_iam_role.unauth_role.id
}

resource aws_iam_role_policy_attachment "auth_s3_policy_attach_read" {
  policy_arn = aws_iam_policy.this.arn
  role       = aws_iam_role.auth_role.id
}

resource aws_cognito_identity_pool_roles_attachment "identity_role_att" {
  identity_pool_id = aws_cognito_identity_pool.this.id

  roles = {
    authenticated   = aws_iam_role.auth_role.arn
    unauthenticated = aws_iam_role.unauth_role.arn
  }
}

resource aws_cognito_user_group "groups" {
  for_each = var.cognito_groups

  name         = each.key
  user_pool_id = aws_cognito_user_pool.this.id
  description  = each.value
  role_arn     = aws_iam_role.auth_role.arn
}
