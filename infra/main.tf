resource aws_cognito_user_pool "this" {
  name = var.user_pool_name
}

resource aws_cognito_user_pool_domain "this" {
  domain       = var.user_pool_domain_name
  user_pool_id = aws_cognito_user_pool.this.id
}

resource aws_cognito_user_pool_client "this" {
  name = var.user_pool_client_name

  user_pool_id                 = aws_cognito_user_pool.this.id
  allowed_oauth_flows          = ["implicit"]
  allowed_oauth_scopes         = ["aws.cognito.signin.user.admin", "email", "openid", "profile"]
  supported_identity_providers = ["COGNITO"]
  callback_urls                = var.user_pool_client_callback_urls
  logout_urls                  = var.user_pool_client_logout_urls

  explicit_auth_flows = [
    "ALLOW_REFRESH_TOKEN_AUTH",
    "ALLOW_USER_SRP_AUTH",
  ]

  allowed_oauth_flows_user_pool_client = true
}

resource aws_cognito_identity_pool "this" {
  identity_pool_name               = var.identity_pool_name
  allow_unauthenticated_identities = true

  cognito_identity_providers {
    client_id     = aws_cognito_user_pool_client.this.id
    provider_name = "cognito-idp.${var.region}.amazonaws.com/${aws_cognito_user_pool.this.id}"
  }
}
