resource aws_s3_bucket "bucket" {
  bucket = var.bucket_name
  tags = {
    cognito = "oauth2 poc"
  }

  cors_rule {
    allowed_methods = ["GET", "HEAD", ]
    allowed_headers = [
      "amz-sdk-invocation-id",
      "amz-sdk-request",
      "authorization",
      "x-amz-content-sha256",
      "x-amz-date",
      "x-amz-security-token",
      "x-amz-user-agent"
    ]
    allowed_origins = ["http://localhost:5000"]
    expose_headers = [
      "x-amz-server-side-encryption",
      "x-amz-request-id",
      "x-amz-id-2",
      "ETag"
    ]
    max_age_seconds = 3000
  }

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "ReadRole",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::237889007525:role/${var.auth_role_name}"
      },
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${var.bucket_name}/*"
    }
  ]
}
POLICY
}
