variable user_pool_name {
  type = string
}

variable user_pool_domain_name {
  type = string
}

variable user_pool_client_name {
  type = string
}

variable user_pool_client_callback_urls {
  type = list
}

variable user_pool_client_logout_urls {
  type = list
}

variable "cognito_groups" {
  type = map
  default = {
    view-only-users = "View only users"
    admin-users     = "Users with admin privileges"
  }
}

variable auth_role_name {
  description = "Authenticated IAM role name"
  type        = string
}

variable unauth_role_name {
  description = "Not Authenticated IAM role name"
  type        = string
}

variable identity_pool_name {
  type = string
}

variable iam_policy_name {
  type = string
}

variable bucket_name {
  type = string
}

variable region {
  type = string
}
