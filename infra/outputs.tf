output domain {
  value = format("%s.auth.%s.amazoncognito.com", aws_cognito_user_pool_domain.this.domain, var.region)
}

output region {
  value = var.region
}

output web_client_id {
  value = aws_cognito_user_pool_client.this.id
}

output user_pool_id {
  value = aws_cognito_user_pool.this.id
}

output identity_pool_id {
  value = aws_cognito_identity_pool.this.id
}

output bucket_name {
  value = aws_s3_bucket.bucket.bucket
}
