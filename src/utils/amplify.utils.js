import { navigate } from 'svelte-routing';
import { storedUser } from '../store';

import { Hub } from '@aws-amplify/core';

export const startAuthListener = () => {
  Hub.listen('auth', ({ payload: { event, data } }) => {
    switch (event) {
      case 'signOut':
        console.log('signOut', data);
        storedUser.set(null);
        break;

      case 'signIn':
        console.log('signIn', data);
        storedUser.set(data.username);
        navigate('/');
        break;

      default:
        console.log('default', event, data);
        break;
    }
  });

};
