import { writable, get } from 'svelte/store';

// Find username key by listing localStorage keys and look for the one containing
// the substr LastAuthUser.
const key = Object.keys(localStorage).find(str => str.includes('LastAuthUser')) || '';
const _username = localStorage.getItem(key);

export const storedUser = writable(_username);

storedUser.subscribe(value => {
  if (value) localStorage.setItem('storedUser', value)
  else localStorage.removeItem('storedUser')
});
