export default {
  Auth: {
    region: process.env.SVELTE_APP_COGNITO_REGION,
    userPoolId: process.env.SVELTE_APP_USER_POOL_ID,
    userPoolWebClientId: process.env.SVELTE_APP_USER_POOL_WEB_CLIENT_ID,

    // We also need to set identityPoolId here.
    // Ref: https://github.com/aws-amplify/amplify-js/issues/5213#issuecomment-610377126
    identityPoolId: process.env.SVELTE_APP_COGNITO_IDENTITY_POOL,

    oauth: {
      domain: process.env.SVELTE_APP_COGNITO_DOMAIN,
      scope: ['openid'],
      redirectSignIn: process.env.SVELTE_APP_REDIRECT_SIGN_IN_URL,
      redirectSignOut: process.env.SVELTE_APP_REDIRECT_SIGN_OUT_URL,
      responseType: 'token',
    },
  },
  Storage: {
    AWSS3: {
      bucket: process.env.SVELTE_APP_S3_BUCKET_NAME,
      region: process.env.SVELTE_APP_S3_BUCKET_REGION,
      identityPoolId: process.env.SVELTE_APP_COGNITO_IDENTITY_POOL,
      identityPoolRegion: process.env.SVELTE_APP_COGNITO_REGION,
    }
  },
};
