# AWS Cognito Authentication/Authorization with Svelte

The purpose of this web application is to present how we have implemented authentication and authorization
flows in my current project.

So, you'll find a minimal setup to start playing with.

Here's the project files' tree:

```
$  tree -I 'node_modules|scripts|build' .
.
|-- README.md
|-- infra
|   |-- bucket.tf
|   |-- config.tf
|   |-- iam.tf
|   |-- inputs.tf
|   |-- main.tf
|   |-- outputs.tf
|   `-- terraform.tfvars
|-- package-lock.json
|-- package.json
|-- public
|   |-- dou_bg.jpg
|   |-- favicon.png
|   |-- global.css
|   `-- index.html
|-- rollup.config.js
`-- src
    |-- App.svelte
    |-- aws-exports.js
    |-- changepassword
    |   `-- ChangePassword.svelte
    |-- common
    |   |-- Button.svelte
    |   `-- InfoBox.svelte
    |-- ellipsis
    |   `-- Ellipsis.svelte
    |-- gallery
    |   `-- Gallery.svelte
    |-- home
    |   |-- Home.svelte
    |   `-- Speech.svelte
    |-- login
    |   `-- Login.svelte
    |-- main.js
    |-- store.js
    `-- utils
        |-- Logout.svelte
        `-- amplify.utils.js

10 directories, 29 files
```

# Tech Stack

- Svelte
- Amplify-JS
- AWS Cognito
- AWS S3 Bucket
- Terraform

## Getting started for Devs

```
$ cd cognito-svelte-poc/
$ docker container run --volume `pwd`:/app --interactive --rm --tty --publish 5000:5000 --workdir /app node:12 bash
$ cp .env.example .env
$ npm install
$ npm run dev
```

Previous commands will expose port 5000, so user can play with the application at localhost:5000.
